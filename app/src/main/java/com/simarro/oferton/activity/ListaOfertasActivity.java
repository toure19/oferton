package com.simarro.oferton.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.simarro.oferton.R;
import com.simarro.oferton.bd.MiBD;
import com.simarro.oferton.common.DialogoConfirmacion;
import com.simarro.oferton.fragment.ListaOfertasFragment;
import com.simarro.oferton.fragment.MapaFragment;
import com.simarro.oferton.fragment.NuevaOfertaFragment;
import com.simarro.oferton.fragment.SettingsFragment;
import com.simarro.oferton.fragment.ValidarOfertaFragment;
import com.simarro.oferton.model.Oferta;
import com.simarro.oferton.model.Usuario;

public class ListaOfertasActivity extends AppCompatActivity {

    private Toolbar appbar;
    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private Fragment fragment;
    private Usuario usu;
    private TextView txtUsuario;
    private MiBD miBD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_ofertas);
        appbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        fragment = new ListaOfertasFragment();
        inflarFragment(fragment);

        usu = (Usuario) getIntent().getSerializableExtra("usuario");
        navView = (NavigationView) findViewById(R.id.navview);
        View headerView = navView.getHeaderView(0);
        txtUsuario = (TextView) headerView.findViewById(R.id.txtUsuario);
        txtUsuario.setText(getResources().getString(R.string.bienvenido) + " " + usu.getNombre()); //saludo usuario navheader

        getSupportActionBar().setTitle(R.string.app_name);
        setTipo(headerView);
        habilitarValidar();
        miBD = MiBD.getInstance(this);

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        fragment = null;
                        boolean salir = false;
                        switch (menuItem.getItemId()) {
                            case R.id.menu_fragment_principal: //Volver al fragment principal
                                fragment = new ListaOfertasFragment();
                                break;
                            case R.id.menu_fragment_nueva:
                                fragment = new NuevaOfertaFragment();
                                break;
                            case R.id.menu_fragment_validar:
                                fragment = new ValidarOfertaFragment();
                                break;
                            case R.id.menu_fragment_localizacion:
                                fragment = new MapaFragment();

                                break;
                            case R.id.menu_settings:
                                fragment = new SettingsFragment();
                                break;
                            case R.id.menu_cerrar:
                                salir = true;
                                Intent intent = new Intent(ListaOfertasActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                        }
                        if (!salir) {
                            inflarFragment(fragment);
                            menuItem.setChecked(true);
                            drawerLayout.closeDrawers();
                        }
                        return true;
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentActivo = getSupportFragmentManager().getFragments().get(0);
        if (!(fragmentActivo instanceof ListaOfertasFragment)) {
            fragment = new ListaOfertasFragment();
            inflarFragment(fragment);
        } else {
            DialogoConfirmacion dialogoConfirmacion = new DialogoConfirmacion();
            dialogoConfirmacion.show(getSupportFragmentManager(), "Cerrar app");
        }
    }

    public void inflarFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    public boolean esAdmin() {
        return (usu.getTipo().equals("admin"));
    }

    public void habilitarValidar() {
        if (esAdmin())
            navView.getMenu().getItem(2).setVisible(true);
    }

    public void setTipo(View v) {
        setTipoLogo(v);
        setTipoText(v);
    }

    public void setTipoLogo(View v) {
        ImageView img = v.findViewById(R.id.imgTipo);
        if (esAdmin()) {
            img.setImageResource(R.drawable.admin);
        } else {
            img.setImageResource(R.drawable.user);
        }
    }

    public void setTipoText(View v) {
        TextView txt = v.findViewById(R.id.txtTipo);
        if (esAdmin()) {
            txt.setText(getResources().getString(R.string.administrador));
        } else {
            txt.setText(getResources().getString(R.string.usuario));
        }
    }

    public void validarOferta(View v) {
        LinearLayout linearLayout = (LinearLayout) v.getParent();
        Oferta o = new Oferta();
        o.setId(Integer.parseInt(((TextView) linearLayout.findViewById(R.id.lblId)).getText().toString()));
        o = (Oferta) miBD.getOfertaDAO().search(o);
        o.setActivo(1);
        miBD.getOfertaDAO().update(o);
        Fragment fragment = new ValidarOfertaFragment();
        inflarFragment(fragment);
        Toast.makeText(this, getResources().getString(R.string.validadaOk), Toast.LENGTH_SHORT).show();
    }

    public void borrarOferta(View v) {
        LinearLayout linearLayout = (LinearLayout) v.getParent();
        Oferta o = new Oferta();
        o.setId(Integer.parseInt(((TextView) linearLayout.findViewById(R.id.lblId)).getText().toString()));
        o = (Oferta) miBD.getOfertaDAO().search(o);
        miBD.getOfertaDAO().delete(o);
        Fragment fragment = new ValidarOfertaFragment();
        inflarFragment(fragment);
        Toast.makeText(this, getResources().getString(R.string.borradaOk), Toast.LENGTH_SHORT).show();
    }

}
