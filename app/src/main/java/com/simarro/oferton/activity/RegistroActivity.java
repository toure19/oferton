package com.simarro.oferton.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.simarro.oferton.R;
import com.simarro.oferton.bd.MiBD;
import com.simarro.oferton.model.Usuario;

import java.util.ArrayList;

public class RegistroActivity extends AppCompatActivity {
    private EditText nombre;
    private EditText correoElectronico;
    private EditText pass;
    private EditText passConf;
    private Button btnRegistro;
    private MiBD miBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        miBD = MiBD.getInstance(this);
        nombre = findViewById(R.id.txtNombreRegistro);
        correoElectronico = findViewById(R.id.txtUsuarioRegistro);
        pass = findViewById(R.id.txtPassRegistro);
        passConf = findViewById(R.id.txtPassConfRegistro);
        btnRegistro = findViewById(R.id.btnRegistro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allOk = true;
                int numMsg = 0;
                ArrayList<String> listaMensajes = new ArrayList<>();
                String msg;
                if (nombre.getText().toString().length() <= 0) {
                    msg = "Debes rellenar el campo nombre";
                    listaMensajes.add(msg);
                    allOk = false;
                    numMsg++;
                }
                if (correoElectronico.getText().toString().length() <= 0) {
                    msg = "Debes rellenar el campo correo electrónico";
                    listaMensajes.add(msg);
                    allOk = false;
                    numMsg++;
                }
                if (pass.getText().toString().length() <= 0 || passConf.getText().toString().length() <= 0) {
                    msg = "Debes rellenar los campos contraseña y confirmar contraseña";
                    listaMensajes.add(msg);
                    allOk = false;
                    numMsg++;
                }
                if (pass.getText().toString().equals(passConf.getText().toString())) {
                    msg = "Las contraseñas deben coincidir";
                    listaMensajes.add(msg);
                    numMsg++;
                }
                if (allOk) { //registramos usuario y iniciamos sesion
                    msg = "Usuario creado correctamente";
                    listaMensajes.add(msg);
                    numMsg++;
                    Usuario usuario = new Usuario(nombre.getText().toString(), correoElectronico.getText().toString(), pass.getText().toString(), "user"); //por defecto user
                    miBD.getUsuarioDAO().add(usuario);
                    Intent intent = new Intent(RegistroActivity.this, ListaOfertasActivity.class);
                    intent.putExtra("usuario", usuario);
                    startActivity(intent); //lanzamos nueva activity
                    finish(); //cerramos activity login
                }
                for (int i = 0; i < numMsg; i++) {
                    Toast.makeText(RegistroActivity.this, listaMensajes.get(i), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
