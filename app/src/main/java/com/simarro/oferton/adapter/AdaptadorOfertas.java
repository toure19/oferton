package com.simarro.oferton.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.simarro.oferton.R;
import com.simarro.oferton.model.Oferta;

import java.util.ArrayList;

public class AdaptadorOfertas extends ArrayAdapter<Oferta> {
    ArrayList<Oferta> ofertas;
    int layout;

    public AdaptadorOfertas(Context context, ArrayList<Oferta> ofertas, int layout) {
        super(context, layout, ofertas);
        this.layout = layout;
        this.ofertas = ofertas;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(layout, null);

        TextView lblTitulo = (TextView) v.findViewById(R.id.lblTitulo);
        lblTitulo.setText(String.valueOf(ofertas.get(position).getTitulo().toUpperCase()));

        ImageView imgImagen = (ImageView) v.findViewById(R.id.imgImagen);
        imgImagen.setImageURI(Uri.parse(ofertas.get(position).getImagen()));

        TextView lblPrecioRecomendado = (TextView) v.findViewById(R.id.lblPrecioRecomendado);
        lblPrecioRecomendado.setText(getContext().getResources().getString(R.string.antes) +": "+ String.valueOf(ofertas.get(position).getPrecioRecomendado() + "€"));
        lblPrecioRecomendado.setPaintFlags(lblPrecioRecomendado.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        TextView lblPrecioOferta = (TextView) v.findViewById(R.id.lblPrecioOferta);
        lblPrecioOferta.setText(getContext().getResources().getString(R.string.ahora) +": "+ String.valueOf(ofertas.get(position).getPrecioOferta() + "€"));

        TextView id = (TextView) v.findViewById(R.id.lblId);
        id.setText(String.valueOf(ofertas.get(position).getId()));

        return (v);
    }

}
