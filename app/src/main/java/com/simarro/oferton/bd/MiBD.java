package com.simarro.oferton.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.simarro.oferton.dao.OfertaDAO;
import com.simarro.oferton.dao.UsuarioDAO;

public class MiBD extends SQLiteOpenHelper {

    private static SQLiteDatabase db;
    //nombre de la base de datos
    private static final String database = "Oferton";
    //versión de la base de datos
    private static final int version = 1;
    //Instrucción SQL para crear la tabla de Clientes
    private String sqlCreacionOfertas = "CREATE TABLE ofertas ( id INTEGER PRIMARY KEY AUTOINCREMENT, titulo STRING, enlace STRING, descripcion STRING, imagen STRING, precioRecomendado STRING, precioOferta STRING, activo INTEGER);";
    private String sqlCreacionUsuarios = "CREATE TABLE usuarios ( id INTEGER PRIMARY KEY AUTOINCREMENT, nombre STRING, correoElectronico STRING, pass STRING, tipo STRING);";

    private static MiBD instance = null;

    private static OfertaDAO ofertaDAO;
    private static UsuarioDAO usuarioDAO;

    public OfertaDAO getOfertaDAO() {
        return ofertaDAO;
    }

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public static MiBD getInstance(Context context) {
        if (instance == null) {
            instance = new MiBD(context);
            db = instance.getWritableDatabase();
            ofertaDAO = new OfertaDAO();
            usuarioDAO = new UsuarioDAO();
        }
        return instance;
    }

    public static SQLiteDatabase getDB() {
        return db;
    }

    public static void closeDB() {
        db.close();
    }

    ;

    /**
     * Constructor de clase
     */
    protected MiBD(Context context) {
        super(context, database, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreacionOfertas);
        db.execSQL(sqlCreacionUsuarios);

        insercionDatos(db);
        Log.i("SQLite", "Se crea la base de datos " + database + " version " + version);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("SQLite", "Control de versiones: Old Version=" + oldVersion + " New Version= " + newVersion);
        if (newVersion > oldVersion) {
            //elimina tabla
            db.execSQL("DROP TABLE IF EXISTS ofertas");
            db.execSQL("DROP TABLE IF EXISTS usuarios");
            //y luego creamos la nueva tabla
            db.execSQL(sqlCreacionOfertas);
            db.execSQL(sqlCreacionUsuarios);

            insercionDatos(db);
            Log.i("SQLite", "Se actualiza versión de la base de datos, New version= " + newVersion);
        }
    }

    private void insercionDatos(SQLiteDatabase db) {
        // Insertamos las ofertas
        db.execSQL("INSERT INTO ofertas(id, titulo, enlace, descripcion, imagen, precioRecomendado,precioOferta,activo) VALUES (1, 'Kawasaki Z750', 'https://www.autoscout24.es/moto/kawasaki/kawasaki-z/kawasaki-z-750/kawasaki-z-750-r/', 'Kawasaki Z750 de ocasión.', 'content://media/external/images/media/24', '8000','4000','1');");
        db.execSQL("INSERT INTO ofertas(id, titulo, enlace, descripcion, imagen, precioRecomendado,precioOferta,activo) VALUES (2, 'Ferrari 360', 'https://www.motor.es/coches-segunda-mano/lugo/ferrari-360-17101261.html', 'Ferrari de ocasiÓn.', 'content://media/external/images/media/23', '100000','49900','1');");
        db.execSQL("INSERT INTO ofertas(id, titulo, enlace, descripcion, imagen, precioRecomendado,precioOferta,activo) VALUES (3, 'Oferta prueba', 'https://www.amazon.es', 'PRUEBA.', 'content://media/external/images/media/25', '200','120','0');");

        //Insertamos los usuarios
        db.execSQL("INSERT INTO usuarios(id, nombre, correoElectronico, pass, tipo) VALUES (1, 'Aitor', 'aitor@gmail.com', '1234','admin');");
        db.execSQL("INSERT INTO usuarios(id, nombre, correoElectronico, pass, tipo) VALUES (2, 'Pepe', 'pepe@gmail.com', '1234','user');");

        // Resto de tablas
    }


}