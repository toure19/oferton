package com.simarro.oferton.bd;

import android.content.Context;

import com.simarro.oferton.model.Oferta;
import com.simarro.oferton.model.Usuario;

import java.util.ArrayList;


public class OfertonOperacional {


    private MiBD miBD;

    protected OfertonOperacional(Context context) {
        miBD = MiBD.getInstance(context);
    }

    private static OfertonOperacional instance = null;

    public static OfertonOperacional getInstance(Context context) {
        if (instance == null) {
            instance = new OfertonOperacional(context);
        }
        return instance;
    }

    // Operacion Login: Verifica que el usuario existe y que su contraseña es correcta. Recibira un usuario
    // que solo contendrá el correo y la password.
    public Usuario login(Usuario u) {
        Usuario aux = (Usuario) miBD.getUsuarioDAO().search(u);
        if (aux == null) {
            return null;
        } else if (aux.getPass().equals(u.getPass())) {
            return aux;
        } else {
            return null;
        }
    }

    // Operacion changePassword: Cambia la password del cliente. Recibirá el cliente de la aplicación con la password cambiada.
    // Si devuelve un 1 es que ha verificado el cambio de password como correcto y todo ha ido bien, mientras que si devuelve
    // mientras que si devuelve un 0 no ha verificado el cambio de password como correcto y ha habido un error al cambiarlo.
    /*public int changePassword(Cliente c) {
        int resultado = miBD.getClienteDAO().update(c);
        if (resultado == 0) {
            return 0;
        } else {
            return 1;
        }

    }*/

    // Operacion getOfertas: Obtiene un ArrayList de todas las ofertas
    public ArrayList<Oferta> getOfertas() {
        return miBD.getOfertaDAO().getAll();
    }

}
