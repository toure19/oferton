package com.simarro.oferton.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.widget.GridView;
import android.widget.TextView;
import com.simarro.oferton.R;
import com.simarro.oferton.adapter.AdaptadorOfertas;
import com.simarro.oferton.bd.OfertonOperacional;
import com.simarro.oferton.model.Oferta;

import java.util.ArrayList;
import java.util.Locale;

public class Tools {
    public static void setNumeroColumnas(GridView gridView, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int num = pref.getInt("numColumnas", 0) + 1;
        gridView.setNumColumns(num);
    }

    public static void mostrarOfertas(Activity activity, GridView gridView, int layout, int estado) {
        Context context = activity.getApplicationContext();
        OfertonOperacional oo = OfertonOperacional.getInstance(context);
        ArrayList<Oferta> listaOfertas = oo.getOfertas(); //obtenemos ofertas
        ArrayList<Oferta> listaOfertasFiltrada = filtrarOfertas(listaOfertas, estado); //filtramos ofertas
        setNumeroOfertas(activity, listaOfertasFiltrada);
        AdaptadorOfertas adapter = new AdaptadorOfertas(activity, listaOfertasFiltrada, layout);
        gridView.setAdapter(adapter);
    }

    public static ArrayList<Oferta> filtrarOfertas(ArrayList<Oferta> listaOfertas, int estado) {
        ArrayList<Oferta> listaOfertasFiltrada = new ArrayList<>();
        for (Oferta o : listaOfertas) {
            if (o.getActivo() == estado) {
                listaOfertasFiltrada.add(o);
            }
        }
        return listaOfertasFiltrada;
    }

    public static void setNumeroOfertas(Activity activity, ArrayList<Oferta> listaOfertasFiltrada) {
        TextView txtTotal = activity.findViewById(R.id.txtTotalOfertas);
        if (listaOfertasFiltrada.size() > 0) {
            txtTotal.setText(activity.getApplicationContext().getResources().getString(R.string.total_ofertas).toUpperCase() + ": " + String.valueOf(listaOfertasFiltrada.size())); //seteamos valor de txt ofertas
        } else {
            txtTotal.setText(activity.getApplicationContext().getResources().getString(R.string.no_ofertas).toUpperCase()); //seteamos txt no hay ofertas
        }
    }

    public static void setLanguage(Context context) {
        String languageToLoad;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (sharedPreferences.getInt("idioma", 0) == 1) {
            languageToLoad = "en"; // english
        } else {
            languageToLoad = "es"; // spanish

        }
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }
}
