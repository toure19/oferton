package com.simarro.oferton.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.simarro.oferton.bd.MiBD;
import com.simarro.oferton.model.Oferta;

import java.util.ArrayList;


public class OfertaDAO implements PojoDAO {
    @Override
    public long add(Object obj) {
        ContentValues contentValues = new ContentValues();
        Oferta o = (Oferta) obj;
        contentValues.put("titulo", o.getTitulo());
        contentValues.put("enlace", o.getEnlace());
        contentValues.put("descripcion", o.getDescripcion());
        contentValues.put("imagen", o.getImagen());
        contentValues.put("precioRecomendado", o.getPrecioRecomendado());
        contentValues.put("precioOferta", o.getPrecioOferta());
        contentValues.put("activo",o.getActivo());

        return MiBD.getDB().insert("ofertas", null, contentValues);
    }

    @Override
    public int update(Object obj) {
        ContentValues contentValues = new ContentValues();
        Oferta o = (Oferta) obj;
        contentValues.put("titulo", o.getTitulo());
        contentValues.put("enlace", o.getEnlace());
        contentValues.put("descripcion", o.getDescripcion());
        contentValues.put("imagen", o.getImagen());
        contentValues.put("precioRecomendado", o.getPrecioRecomendado());
        contentValues.put("precioOferta", o.getPrecioOferta());
        contentValues.put("activo",o.getActivo());

        String condicion = "id=" + String.valueOf(o.getId());

        return MiBD.getDB().update("ofertas", contentValues, condicion, null);
    }

    @Override
    public void delete(Object obj) {
        Oferta o = (Oferta) obj;
        String condicion = "id=" + String.valueOf(o.getId());

        //Se borra el producto indicado en el campo de texto
        MiBD.getDB().delete("ofertas", condicion, null);
    }

    @Override
    public Object search(Object obj) {
        Oferta o = (Oferta) obj;
        String condicion = "id=" + String.valueOf(o.getId());
        String[] columnas = {
                "id", "titulo", "enlace", "descripcion", "imagen", "precioRecomendado", "precioOferta","activo"
        };
        Cursor cursor = MiBD.getDB().query("ofertas", columnas, condicion, null, null, null, null);
        if (cursor.moveToFirst()) {
            o.setId(cursor.getInt(0));
            o.setTitulo(cursor.getString(1));
            o.setEnlace(cursor.getString(2));
            o.setDescripcion(cursor.getString(3));
            o.setImagen(cursor.getString(4));
            o.setPrecioRecomendado(cursor.getDouble(5));
            o.setPrecioOferta(cursor.getDouble(6));
            o.setActivo(cursor.getInt(7));

            return o;
        } else {
            return null;
        }
    }

    @Override
    public ArrayList getAll() {
        ArrayList<Oferta> listaOfertas = new ArrayList<Oferta>();
        String[] columnas = {
                "id", "titulo", "enlace", "descripcion", "imagen", "precioRecomendado", "precioOferta","activo"
        };
        Cursor cursor = MiBD.getDB().query("ofertas", columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                Oferta o = new Oferta();
                o.setId(cursor.getInt(0));
                o.setTitulo(cursor.getString(1));
                o.setEnlace(cursor.getString(2));
                o.setDescripcion(cursor.getString(3));
                o.setImagen(cursor.getString(4));
                o.setPrecioRecomendado(cursor.getDouble(5));
                o.setPrecioOferta(cursor.getDouble(6));
                o.setActivo(cursor.getInt(7));

                listaOfertas.add(o);
            } while (cursor.moveToNext());
        }
        return listaOfertas;
    }

}
