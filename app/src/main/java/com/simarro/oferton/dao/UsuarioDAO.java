package com.simarro.oferton.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.simarro.oferton.bd.MiBD;
import com.simarro.oferton.model.Oferta;
import com.simarro.oferton.model.Usuario;

import java.util.ArrayList;


public class UsuarioDAO implements PojoDAO {
    @Override
    public long add(Object obj) {
        ContentValues contentValues = new ContentValues();
        Usuario u = (Usuario) obj;
        contentValues.put("nombre", u.getNombre());
        contentValues.put("correoElectronico", u.getCorreoElectronico());
        contentValues.put("pass", u.getPass());
        contentValues.put("tipo", u.getTipo());

        return MiBD.getDB().insert("usuarios", null, contentValues);
    }

    @Override
    public int update(Object obj) {
        ContentValues contentValues = new ContentValues();
        Usuario u = (Usuario) obj;
        contentValues.put("nombre", u.getNombre());
        contentValues.put("correoElectronico", u.getCorreoElectronico());
        contentValues.put("pass", u.getPass());
        contentValues.put("tipo", u.getTipo());

        String condicion = "id=" + String.valueOf(u.getId());

        return MiBD.getDB().update("usuarios", contentValues, condicion, null);
    }

    @Override
    public void delete(Object obj) {
        Usuario u = (Usuario) obj;
        String condicion = "id=" + String.valueOf(u.getId());

        //Se borra el producto indicado en el campo de texto
        MiBD.getDB().delete("usuarios", condicion, null);
    }

    @Override
    public Object search(Object obj) {
        Usuario u = (Usuario) obj;
        String condicion = "id=" + String.valueOf(u.getId());
        String[] columnas = {
                "id", "nombre", "correoElectronico", "pass", "tipo"
        };
        Cursor cursor = MiBD.getDB().query("usuarios", columnas, condicion, null, null, null, null);
        if (cursor.moveToFirst()) {
            u.setId(cursor.getInt(0));
            u.setNombre(cursor.getString(1));
            u.setCorreoElectronico(cursor.getString(2));
            u.setPass(cursor.getString(3));
            u.setTipo(cursor.getString(4));

            return u;
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<Usuario> getAll() {
        ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
        String[] columnas = {
                "id", "nombre", "correoElectronico", "pass","tipo"
        };
        Cursor cursor = MiBD.getDB().query("usuarios", columnas, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                Usuario u = new Usuario();
                u.setId(cursor.getInt(0));
                u.setNombre(cursor.getString(1));
                u.setCorreoElectronico(cursor.getString(2));
                u.setPass(cursor.getString(3));
                u.setTipo(cursor.getString(4));

                listaUsuarios.add(u);
            } while (cursor.moveToNext());
        }
        return listaUsuarios;
    }

}
