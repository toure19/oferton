package com.simarro.oferton.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.simarro.oferton.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MapaFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationManager locationManager;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    public MapaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mapa, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MiAsynk myAsynk = new MiAsynk();
        myAsynk.execute("https://nominatim.openstreetmap.org/search?city=xativa&format=json");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        obtnerPermisosDeLocalizacion();
    }

    /*@Override
    public void onLocationChanged(Location location) {
        Toast toast = Toast.makeText(getContext(), "Cambio de posición: ", Toast.LENGTH_SHORT);
        toast.show();
        mostrarPosicionEnMapa(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast toast = Toast.makeText(getContext(), "Cambio en el estado del proveedor: " + status, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast toast = Toast.makeText(getContext(), "Proveedor habilitado", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast toast = Toast.makeText(getContext(), "Proveedor deshabilitado", Toast.LENGTH_SHORT);
        toast.show();
    }
*/
    /**
     * En caso de no disponer de permisos para obtener datos de localización pregunta al usuario
     */
    private void obtnerPermisosDeLocalizacion(){
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            prepararOpcionesMapa();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Retorno de petición de permisos al usuario
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    prepararOpcionesMapa();
                }
            }
        }
    }

    /**
     * Establece la posición inicial del mapa
     */
    private void prepararOpcionesMapa() {
        try {
            //Habilito el botón para mostrar mi posición
            mMap.setMyLocationEnabled(true);

            //Establecemos el tipo de mapa
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            //Añade controles para hacer zoom
            UiSettings mapSettings = mMap.getUiSettings();
            mapSettings.setZoomControlsEnabled(true);

            locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            mostrarPosicionEnMapa(location);
        }catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Anyade un marker en la posición recibida como parámetro
     *
     * @param location
     */
    private void mostrarPosicionEnMapa(Location location) {
        if(location != null) {
            mMap.clear();
            LatLng miPosicion = new LatLng(location.getLatitude(), location.getLongitude());

            //Posición el retiro Madrid
            //LatLng miPosicion = new LatLng(40.417325, -3.68);

            mMap.addMarker(new MarkerOptions()
                    .position(miPosicion)
                    .title("Marker en posición actual")
                    .snippet("Contenido que se incluye en el snippet")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            //Posicionar cámara sin zoom
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(miPosicion));

            //Posicionar cámara con zoom, inclinación (tilt)
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                    new CameraPosition.Builder()
                            .target(miPosicion)
                            .tilt(70) //inclinación
                            .bearing(45) //orientación
                            .zoom(19) //zoom
                            .build()));
        }
    }


    /**
     * Asynctask usada para realizar la petición a la API de consulta de posicionamiento
     *
     */
    class MiAsynk extends AsyncTask<String, Void, Location>{

        @Override
        protected Location doInBackground(String... strings) {
            List<InfoLocation> listado = new ArrayList<>();
            // Connect to the URL using java's native library
            try {
                String json = readUrl(strings[0]);
                JsonParser jp = new JsonParser(); //from gson
                JsonElement root = jp.parse(json); //Convert the input stream to a json element
                JsonArray arrayObjetos = root.getAsJsonArray();
                Type listType = new TypeToken<ArrayList<InfoLocation>>(){}.getType();
                listado = new Gson().fromJson(arrayObjetos, listType);

            } catch (Exception e) {
                e.printStackTrace();
            }

            if(listado.isEmpty()){
                return null;
            }else{
                Location localizacion = new Location("");//provider name is unnecessary
                localizacion.setLatitude(Double.valueOf(listado.get(0).getLat()));//your coords of course
                localizacion.setLongitude(Double.valueOf(listado.get(0).getLon()));
                return localizacion;
            }
        }

        @Override
        protected void onPostExecute(Location localizacion) {
            if(localizacion == null){
                Toast toast = Toast.makeText(getContext(), "Dirección no válida", Toast.LENGTH_SHORT);
                toast.show();
            }else {
                mostrarPosicionEnMapa(localizacion);
            }
            //reiniciarEditText();
        }
    }

    /**
     * Limpia el contenido del editText y oculta el teclado
     */
    /*private void reiniciarEditText(){
        editText.getText().clear();
        View view = MapsActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }*/

    /**
     * Lee el Json retornado al hacer la consulta a la API y lo transforma a String
     *
     * @param sURL
     * @return
     * @throws Exception
     */
    private String readUrl(String sURL)  throws Exception{
        BufferedReader reader = null;
        try {
            URL url = new URL(sURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            int respuesta = connection.getResponseCode();
            StringBuilder result = new StringBuilder();
            if (respuesta == HttpURLConnection.HTTP_OK){
                InputStream in = new BufferedInputStream(connection.getInputStream());
                reader = new BufferedReader(new InputStreamReader(in));
            }

            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);
            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    /**
     * Clase usada para parsear la respuesta de la API
     */
    class InfoLocation {
        private String lat;
        private String lon;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }
    }

}
