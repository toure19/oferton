package com.simarro.oferton.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.simarro.oferton.R;
import com.simarro.oferton.bd.MiBD;
import com.simarro.oferton.bd.OfertonOperacional;
import com.simarro.oferton.model.Oferta;
import com.simarro.oferton.model.Usuario;

public class NuevaOfertaFragment extends Fragment {
    public static final int PICK_IMAGE = 1;
    private EditText titulo;
    private EditText enlace;
    private EditText descripcion;
    private TextView txtImagen;
    private EditText precio;
    private EditText precioOferta;
    private Button btnCargarImagen;
    private Button btnCrearOferta;
    private String tit;
    private String enl;
    private String des;
    private String rutaImagen;
    private Double prec;
    private Double precOferta;
    private OfertonOperacional oo;

    private boolean ok = true;

    public NuevaOfertaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nueva_oferta, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final MiBD miBD = MiBD.getInstance(getContext());
        oo = OfertonOperacional.getInstance(getContext());
        titulo = getView().findViewById(R.id.txtTituloNueva);
        enlace = getView().findViewById(R.id.txtEnlaceNueva);
        descripcion = getView().findViewById(R.id.txtDescripcionNueva);
        txtImagen = getView().findViewById(R.id.txtImagenNueva);
        precio = getView().findViewById(R.id.txtPrecioNueva);
        precioOferta = getView().findViewById(R.id.txtPrecioOfertaNueva);
        btnCrearOferta = getView().findViewById(R.id.btnCrearOferta);
        btnCargarImagen = getView().findViewById(R.id.btnImagenNueva);
        rutaImagen = "";

        btnCargarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.selecciona_imagen)), PICK_IMAGE);
            }
        });
        btnCrearOferta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobaciones();
                if (ok) {
                    Oferta oferta = new Oferta(tit, enl, des, rutaImagen, prec, precOferta, 0);
                    miBD.getOfertaDAO().add(oferta);
                    Toast.makeText(getContext(), getResources().getString(R.string.falta_validacion), Toast.LENGTH_LONG).show();
                    Fragment fragment = new ListaOfertasFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, fragment)
                            .commit();
                }

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && data != null) {
            Uri selectedImage = data.getData();
            rutaImagen = String.valueOf(selectedImage);
            txtImagen.setText(getResources().getString(R.string.imagen_cargada));
        }
    }

    public void comprobaciones() {
        comprobarTitulo();
        comprobarEnlace();
        comprobarDescripcion();
        comprobarImagen();
        comprobarPrecios();
    }

    public void comprobarTitulo() {
        if (titulo.getText().length() > 0) {
            if (titulo.getText().length() > 6) {
                tit = titulo.getText().toString();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.longitud_minima_titulo), Toast.LENGTH_SHORT).show();
                ok = false;
            }
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.titulo_vacio), Toast.LENGTH_SHORT).show();
            ok = false;
        }
    }

    public void comprobarEnlace() {
        if (enlace.getText().length() > 0) {
            if (enlace.getText().length() > 10) {
                enl = enlace.getText().toString();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.longitud_minima_enlace), Toast.LENGTH_SHORT).show();
                ok = false;
            }
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.enlace_vacio), Toast.LENGTH_SHORT).show();
            ok = false;
        }
    }

    public void comprobarDescripcion() {
        if (descripcion.getText().length() > 0) {
            if (descripcion.getText().length() > 20) {
                des = descripcion.getText().toString();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.longitud_minima_descripcion), Toast.LENGTH_SHORT).show();
                ok = false;
            }
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.descripcion_vacio), Toast.LENGTH_SHORT).show();
            ok = false;
        }
    }

    public void comprobarImagen() {
        if (rutaImagen.isEmpty()) {
            Toast.makeText(getContext(), getResources().getString(R.string.no_imagen), Toast.LENGTH_SHORT).show();
            ok = false;
        }
    }

    public void comprobarPrecios() {
        if (precio.getText().toString().length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.precio_rec_vacio), Toast.LENGTH_SHORT).show();
            ok = false;
        } else {
            prec = Double.parseDouble(precio.getText().toString());
        }
        if (precioOferta.getText().toString().length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.precio_oferta_vacio), Toast.LENGTH_SHORT).show();
            ok = false;
        } else {
            precOferta = Double.parseDouble(precioOferta.getText().toString());
        }
    }

}
