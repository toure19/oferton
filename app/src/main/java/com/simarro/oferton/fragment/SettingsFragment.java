package com.simarro.oferton.fragment;

import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.simarro.oferton.R;

public class SettingsFragment extends Fragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        final Spinner listNumColumnas = getView().findViewById(R.id.listNumColumnas);
        final Spinner listIdiomas = getView().findViewById(R.id.listIdiomas);
        listNumColumnas.setSelection(pref.getInt("numColumnas", 0));
        listIdiomas.setSelection(pref.getInt("idioma", 0));
        listNumColumnas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int j = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                j++;
                if (j >= 2) {
                    listNumColumnas.setSelection(i);
                    SharedPreferences.Editor prefEditor = pref.edit();
                    prefEditor.putInt("numColumnas", i);
                    prefEditor.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        listIdiomas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int j = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                j++;
                listIdiomas.setSelection(position);
                SharedPreferences.Editor prefEditor = pref.edit();
                prefEditor.putInt("idioma", position);
                prefEditor.commit();
                if (j >= 2) {
                    Toast.makeText(getContext(), getResources().getString(R.string.idiomaOk), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
