package com.simarro.oferton.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.simarro.oferton.R;
import com.simarro.oferton.adapter.AdaptadorOfertas;
import com.simarro.oferton.bd.OfertonOperacional;
import com.simarro.oferton.common.Tools;
import com.simarro.oferton.model.Oferta;

import java.util.ArrayList;

public class ValidarOfertaFragment extends Fragment {
    private ArrayList<Oferta> listaOfertas;
    private ArrayList<Oferta> listaOfertasFiltrada;
    private GridView simpleList;
    private OfertonOperacional oo;
    private AdaptadorOfertas adapter;

    public ValidarOfertaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_validar_oferta, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        oo = OfertonOperacional.getInstance(getContext());
        listaOfertas = new ArrayList<>();
        simpleList = (GridView) getView().findViewById(R.id.simpleGridViewValidar);
        Tools.setNumeroColumnas(simpleList, getContext());
        Tools.mostrarOfertas(getActivity(), simpleList, R.layout.layout_validar_oferta_item, 0);
    }
}
