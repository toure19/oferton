package com.simarro.oferton.fragment;

import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simarro.oferton.R;
import com.simarro.oferton.model.Oferta;

public class VerOfertaFragment extends Fragment {

    private Oferta oferta;
    private TextView txtTitulo;
    private TextView txtEnlace;
    private TextView txtDescripcion;
    private TextView txtPrecioRecomendado;
    private TextView txtPrecioOferta;
    private ImageView imgImagen;

    public VerOfertaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ver_oferta, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        oferta = (Oferta) getArguments().getSerializable("oferta");
        txtTitulo = getView().findViewById(R.id.txtTituloOferta);
        txtEnlace = getView().findViewById(R.id.txtEnlaceOferta);
        txtDescripcion = getView().findViewById(R.id.txtDescripcionOferta);
        txtPrecioRecomendado = getView().findViewById(R.id.txtPrecioRecomendadoOferta);
        txtPrecioOferta = getView().findViewById(R.id.txtPrecioOferta);
        imgImagen = getView().findViewById(R.id.imgImagenOferta);

        txtTitulo.setText(oferta.getTitulo());
        txtEnlace.setText(oferta.getEnlace());
        txtDescripcion.setText(oferta.getDescripcion());
        txtPrecioRecomendado.setText(getResources().getString(R.string.precio_recomendado) + ": " + String.valueOf(oferta.getPrecioRecomendado() + "€"));
        txtPrecioRecomendado.setPaintFlags(txtPrecioRecomendado.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txtPrecioOferta.setText(getResources().getString(R.string.precio_oferta) + ": " + String.valueOf(oferta.getPrecioOferta() + "€"));
        imgImagen.setImageURI(Uri.parse(oferta.getImagen()));
    }


}
