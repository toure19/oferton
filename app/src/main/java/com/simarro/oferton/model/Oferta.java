package com.simarro.oferton.model;

import java.io.Serializable;

public class Oferta implements Serializable {
    private int id;
    private String titulo;
    private String enlace;
    private String descripcion;
    private String imagen;
    private double precioRecomendado;
    private double precioOferta;
    private int activo;

    public Oferta() {
    }

    public Oferta(String titulo, String enlace, String descripcion, String imagen, double precioRecomendado, double precioOferta, int activo) {
        this.titulo = titulo;
        this.enlace = enlace;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.precioRecomendado = precioRecomendado;
        this.precioOferta = precioOferta;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public double getPrecioRecomendado() {
        return precioRecomendado;
    }

    public void setPrecioRecomendado(double precioRecomendado) {
        this.precioRecomendado = precioRecomendado;
    }

    public double getPrecioOferta() {
        return precioOferta;
    }

    public void setPrecioOferta(double precioOferta) {
        this.precioOferta = precioOferta;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
